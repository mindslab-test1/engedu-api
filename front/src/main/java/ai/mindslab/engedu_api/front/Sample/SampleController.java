package ai.mindslab.engedu_api.front.Sample;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class SampleController {

    @RequestMapping("/demo")
    public String sample() {

        log.error("hi...................... just error.");
        return "html/CustomerTestPage";
    }


    @RequestMapping("/uploadWav")
    @ResponseBody
    public String uploadWav(@RequestParam("message")  String message) {

        log.error("uploadWav.................. message = " + message);
        return "hello";
    }

}
