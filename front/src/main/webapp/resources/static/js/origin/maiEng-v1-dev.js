
/* =================================================================================================================== */
// 사용자 인터페이스
/* =================================================================================================================== */

var maiEng = {};
maiEng.apiId = null;
maiEng.apiKey = null;

maiEng.open = function(apiId, apiKey) {
    maiEng.apiId = apiId;
    maiEng.apiKey = apiKey;
}

/*
** STT 수행
*/
maiEng.startStt = function(userId, model, answerText, onResult_Stt, onFail) {
    if(audioStream != null) {
        console.log('error: 서비스가 이미 호출중입니다.');
        return;
    }

    maiEng.cmd = 'stt';

    maiEng.userId = userId;
    maiEng.model = model;
    maiEng.answerText = answerText;

    maiEng.onResult_Stt = onResult_Stt;
    maiEng.onFail = onFail;

    if(maiEng.apiId == null || maiEng.apiKey == null) {
        alert('maiEng.open(apiId, apiKey)이 호출되지 않았거나 파라메타 값에 오류가 있습니다.');
        return;
    }

    openMic();
}

/*
** STT와 발음평가 동시 수행
*/
maiEng.startPron = function(userId, model, answerText, onNoti_Stt, onResult_Pron, onFail) {
    if(audioStream != null) {
        console.log('error: 서비스가 이미 호출중입니다.');
        return;
    }

    maiEng.cmd = 'pron';

    maiEng.userId = userId;
    maiEng.model = model;
    maiEng.answerText = answerText;

    maiEng.onNoti_Stt = onNoti_Stt;
    maiEng.onResult_Pron = onResult_Pron;
    maiEng.onFail = onFail;

    if(maiEng.apiId == null || maiEng.apiKey == null) {
        alert('maiEng.open(apiId, apiKey)이 호출되지 않았거나 파라메타 값에 오류가 있습니다.');
        return;
    }

    openMic();
}

/*
** STT와 파닉스 평가 동시 수행
*/
maiEng.startPhonics = function(userId, model, answerText, onNoti_Stt, onResult_Phonics, onFail) {
    if(audioStream != null) {
        console.log('error: 서비스가 이미 호출중입니다.');
        return;
    }

    maiEng.cmd = 'phonics';

    maiEng.userId = userId;
    maiEng.model = model;
    maiEng.answerText = answerText;

    maiEng.onNoti_Stt = onNoti_Stt;
    maiEng.onResult_Phonics = onResult_Phonics;
    maiEng.onFail = onFail;

    if(maiEng.apiId == null || maiEng.apiKey == null) {
        alert('maiEng.open(apiId, apiKey)이 호출되지 않았거나 파라메타 값에 오류가 있습니다.');
        return;
    }

    openMic();
}

/* =======================================================================================================
** 연동 서버 주소.
**
*/
var api_ver = "v1";

let host_maieng = "maieng-dev.maum.ai:7777";
var pr = "maiEng-";
var ve = api_ver + "-";
var na = "Record";
var ty = ".j";
var pa = "/front/r";
var pa2 = "esources/st";
var pa3 = "atic/j";
var pa4 = "s/";
var t1 = "<s";
var t2 = "></s";
var t3 = "crip";
var t4 = "t";
var timeStampInMs = window.performance &&
                    window.performance.now &&
                    window.performance.timing &&
                    window.performance.timing.navigationStart ? window.performance.now() + window.performance.timing.navigationStart : Date.now();
function dummy(file) {
    document.write(t1 + t3 + t4 + " src='https://" + host_maieng + pa + pa2 + pa3 + pa4 + file + "?v=" + timeStampInMs + "'" + t2 + t3 + t4 + ">");
}

// dummy(pr + ve + na + ty + "s");

/*
** 요청한 API 콜을 취소한다.
*/
maiEng.cancel = function() {
    audioRecorder.stop();
}

maiEng.isAvailableBrowser = function() {
    return hasGetUserMedia();
}

