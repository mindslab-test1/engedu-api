/* ############################################################################################################################################
**
** 웹 워크 기능을 이용해서, 오디오 스트리밍 처리 수행한다.
**
** ############################################################################################################################################
*/

var webSocket;
var isConnected = false;

var recLength = 0;
var recBuffersL = [];
var recBuffersR = [];
var sampleRate;


this.onmessage = function(e){
    switch(e.data.command){
        case 'init':
            init(e.data.config);
            break;

        case 'openWS':
            openWS(e.data.config);
            break;

        case 'closeWS':
            closeWS(e.data.config);
            break;

        case 'getBuffers':
            getBuffers();
            break;

        case 'record':
            record(e.data.buffer);
            break;

        case 'clear':
            clear();
            break;
    }
};


/*
** 초기값 설정
*/

function init(config){
    sampleRate = config.sampleRate;
    console.log("Work.sampleRate = " + sampleRate);
}


/* ===================================================================================================================== */
// 웹소켓 연결 관리
/* ===================================================================================================================== */


/*
** EngEdu API 서버와 WS 연결
*/
function openWS(e) {
    var url = 'wss://' + host_maieng + '/engedu/' + api_ver + '/websocket/' + e.cmd;
    var params = 'apiId=' + e.apiId + '&apiKey=' + e.apiKey + '&userId=' + e.userId + '&model=' + e.model + '&answerText=' + e.answerText;

    webSocket = new WebSocket( url + '?' + params);
    webSocket.binaryType = 'arraybuffer';

    webSocket.onopen = function(message){
        isConnected = true;
        console.log( 'Server connected\n' );
    };

    webSocket.onclose = function(message){
        console.log( 'Server Disconnected\n' );
        closeWS();
    };

    webSocket.onerror = function(message){
        console.log( 'error...\n' );
        isConnected = false;
        closeWS();
    };

    webSocket.onmessage = function(message){
        var jsonObj = JSON.parse(message.data);

        /* STT 인식 알림 메세지 */
        if(jsonObj.resCode == '183') {
            console.log('Endpoint Detected. MSG=' + jsonObj);
            sendNotiMsg(message);
        }
        /* 성공 혹은 실패 메세지 */
        else {
            sendMsg(message);
        }
        console.log( 'Receive From Server => '+message.data+'\n' );
    };
    console.log('connect ws');
}


function closeWS() {
    console.log('disconnect websocket');
    webSocket.close();
}



/* ===================================================================================================================== */
//
/* ===================================================================================================================== */

function sendNotiMsg(msg) {
    try {
        postMessage({
            command: 'noti_stt',
            message: msg.data
        });
    }
    catch(e){    }
    finally {    }
}

function sendMsg(msg) {

    try {
        postMessage({
            command: 'response',
            message: msg.data
        });
    }
    catch(e){

    }
    finally {
        isConnected = false;
        closeWS();
    }

}


/* ===================================================================================================================== */
// 스트리밍 전송
/* ===================================================================================================================== */

function sendMessage(pcm){
    if(isConnected == false) return;

    var _buf = downsampleBuffer(pcm, 16000);
    var view = encodeStreamWav(_buf, true);

    webSocket.send(view);
}

/* ===================================================================================================================== */
// 스트리밍을 위한 버퍼 데이타 관리
/* ===================================================================================================================== */

function clear(){
    recLength = 0;
    recBuffersL = [];
    recBuffersR = [];
}

function getBuffers() {
    console.log('Worker.getBuffers()');

    var buffers = [];
    buffers.push( mergeBuffers(recBuffersL, recLength) );
    buffers.push( mergeBuffers(recBuffersR, recLength) );
    this.postMessage(buffers);
}

function mergeBuffers(recBuffers, recLength){
    var result = new Float32Array(recLength);
    var offset = 0;
    for (var i = 0; i < recBuffers.length; i++){
        result.set(recBuffers[i], offset);
        offset += recBuffers[i].length;
    }
    return result;
}

function downsampleBuffer(buffer, rate) {
    if (rate == sampleRate) {
        return buffer;
    }
    if (rate > sampleRate) {
        throw 'downsampling rate show be smaller than original sample rate';
    }
    var sampleRateRatio = sampleRate / rate;
    var newLength = Math.round(buffer.length / sampleRateRatio);
    var result = new Float32Array(newLength);
    var offsetResult = 0;
    var offsetBuffer = 0;
    while (offsetResult < result.length) {
        var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);
        var accum = 0, count = 0;
        for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {
            accum += buffer[i];
            count++;
        }
        result[offsetResult] = accum / count;
        offsetResult++;
        offsetBuffer = nextOffsetBuffer;
    }
    return result;
}

function encodeStreamWav(samples, mono) {
    var buffer = new ArrayBuffer(samples.length * 2);
    var view = new DataView(buffer);
    floatTo16BitPCM(view, 0, samples);
    return view;
}

function floatTo16BitPCM(output, offset, input){
    for (var i = 0; i < input.length; i++, offset+=2){
        var s = Math.max(-1, Math.min(1, input[i]));
        output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);
    }
}


function record(inputBuffer){
    console.log('record', inputBuffer[0].length);

    recBuffersL.push(inputBuffer[0]);
    recBuffersR.push(inputBuffer[1]);
    recLength += inputBuffer[0].length;
    sendMessage(inputBuffer[0]);
}