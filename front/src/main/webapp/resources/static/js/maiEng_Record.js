

/* =================================================================================================================== */
// 성공 결과 메세지 처리
/* =================================================================================================================== */

function procResponse_Stt(message) {
    var jsonObj = JSON.parse(message);

    var result = {};
    result.answerText = jsonObj.result.answerText;
    result.userText = jsonObj.result.resultText;
    result.recordUrl = jsonObj.result.recordUrl;
    result.score = jsonObj.result.sentenceScore;

    maiEng.onResult_Stt(result);
}

function procNoti_Stt(message) {
    var jsonObj = JSON.parse(message);

    var result = {};
    result.answerText = jsonObj.result.answerText;
    result.userText = jsonObj.result.resultText;
    result.recordUrl = jsonObj.result.recordUrl;
    result.score = jsonObj.result.sentenceScore;

    maiEng.onNoti_Stt(result);
}

function procResponse_Pron(message) {
    var jsonObj = JSON.parse(message);

    var result = {};
    result.totalScore = jsonObj.result.totalScore;
    result.pronScore = jsonObj.result.pronScore;
    result.speedScore = jsonObj.result.speedScore;
    result.rhythmScore = jsonObj.result.rhythmScore;
    result.intonationScore = jsonObj.result.intonationScore;
    result.segmentalScore = jsonObj.result.segmentalScore;
    result.intonationList = jsonObj.result.intonationList;

    maiEng.onResult_Pron(result);
}

function procResponse_Phonics(message) {
    var jsonObj = JSON.parse(message);

    var result = {};
    result.status = jsonObj.result.status;
    result.userPron = jsonObj.result.userPron;
    result.correctPron = jsonObj.result.correctPron;

    maiEng.onResult_Phonics(result);
}

/* =================================================================================================================== */
// 오디오 설정
/* =================================================================================================================== */

var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext;

var audioInput = null;
var inputPoint = null;
var audioRecorder = null;
var audioStream = null;

var analyser = null;

/*
** 마이크 제어 가능 여부 확인
*/
function hasGetUserMedia() {
    return !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
}


/*
** 마이크 권한 획득
*/
function openMic() {
    audioContext = new AudioContext();

    if (!navigator.getUserMedia) navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    if (!navigator.cancelAnimationFrame) navigator.cancelAnimationFrame = navigator.webkitCancelAnimationFrame || navigator.mozCancelAnimationFrame;
    if (!navigator.requestAnimationFrame) navigator.requestAnimationFrame = navigator.webkitRequestAnimationFrame || navigator.mozRequestAnimationFrame;

    console.log('sample rate: ' + audioContext.sampleRate);
    //alert('sample rate: ' + audioContext.sampleRate);
    navigator.getUserMedia(
        {
            "audio": {
                "mandatory": {
                    "googEchoCancellation": "false",
                    "googAutoGainControl": "false",
                    "googNoiseSuppression": "true",
                    "googHighpassFilter": "true"
                },
                "optional": []
            },
        }, gotStream, function(e) {
            alert('Error getting mic resource.');
            console.log(e);
        });
}

function gotStream(stream) {
    audioStream = stream;

    /* 오디오 자원 해지 */
    audioStream.stop = function() {
        this.getTracks().forEach(function (track) {
            track.stop();
            audioStream = null;
        });
    };

    inputPoint = audioContext.createGain();
    inputPoint.gain.value = 5;
    audioInput = audioContext.createMediaStreamSource(stream);
    audioInput.connect(inputPoint);

    // analyser = audioContext.createAnalyser();
    // analyser.fftSize = 2048;
    // audioInput.connect(analyer);

    audioRecorder = new Recorder( inputPoint );

    zeroGain = audioContext.createGain();
    zeroGain.gain.value = 0;
    inputPoint.connect( zeroGain );

    zeroGain.connect( audioContext.destination );

    console.log('mic ok!!!!');

    audioRecorder.record(maiEng.cmd, maiEng.apiId, maiEng.apiKey, maiEng.userId, maiEng.answerText);
}



/* =================================================================================================================== */
//
/* =================================================================================================================== */

(function(window){

    var Recorder = function(source, cfg){
        var config = cfg || {};
        var bufferLen = config.bufferLen || 16384;
        this.context = source.context;
        if(!this.context.createScriptProcessor){
            this.node = this.context.createJavaScriptNode(bufferLen, 2, 2);
        } else {
            this.node = this.context.createScriptProcessor(bufferLen, 2, 2);
        }

        function handleMessageFromWorker(e) {

            switch(e.data.command) {
                case 'connected':
                    console.log('maieng.connected');
                    break;

                case 'noti_stt':
                    console.log('maieng.noti_stt');

                    if(audioStream != null) audioStream.stop();
                    audioStream = null;
                    audioRecorder.context.suspend();

                    procNoti_Stt(e.data.message);
                    break;

                case 'response':
                    console.log('maieng.response');
                    audioRecorder.stop();

                    if(maiEng.cmd == 'stt') procResponse_Stt(e.data.message);
                    else if(maiEng.cmd == 'pron') procResponse_Pron(e.data.message);
                    else if(maiEng.cmd == 'phonics') procResponse_Phonics(e.data.message);

                    break;
            };
        }



        // var worker = new Worker(config.workerPath || WORKER_PATH);

        var blob = new Blob([
/* --------------------------------------- */
"/* ############################################################################################################################################\n" +
"**\n" +
"** 웹 워크 기능을 이용해서, 오디오 스트리밍 처리 수행한다.\n" +
"**\n" +
"** ############################################################################################################################################\n" +
"*/\n" +
"\n" +
"var webSocket;\n" +
"var isConnected = false;\n" +
"\n" +
"var recLength = 0;\n" +
"var recBuffersL = [];\n" +
"var recBuffersR = [];\n" +
"var sampleRate;\n" +
"\n" +
"\n" +
"this.onmessage = function(e){\n" +
"    switch(e.data.command){\n" +
"        case 'init':\n" +
"            init(e.data.config);\n" +
"            break;\n" +
"\n" +
"        case 'openWS':\n" +
"            openWS(e.data.config);\n" +
"            break;\n" +
"\n" +
"        case 'closeWS':\n" +
"            closeWS(e.data.config);\n" +
"            break;\n" +
"\n" +
"        case 'getBuffers':\n" +
"            getBuffers();\n" +
"            break;\n" +
"\n" +
"        case 'record':\n" +
"            record(e.data.buffer);\n" +
"            break;\n" +
"\n" +
"        case 'clear':\n" +
"            clear();\n" +
"            break;\n" +
"    }\n" +
"};\n" +
"\n" +
"\n" +
"/*\n" +
"** 초기값 설정\n" +
"*/\n" +
"\n" +
"function init(config){\n" +
"    sampleRate = config.sampleRate;\n" +
"}\n" +
"\n" +
"\n" +
"/* ===================================================================================================================== */\n" +
"// 웹소켓 연결 관리\n" +
"/* ===================================================================================================================== */\n" +
"\n" +
"\n" +
"/*\n" +
"** EngEdu API 서버와 WS 연결\n" +
"*/\n" +
"function openWS(e) {\n" +
"    var url = 'wss://maieng.maum.ai:7777/engedu/websocket/' + e.cmd;\n" +
"    var params = 'apiId=' + e.apiId + '&apiKey=' + e.apiKey + '&userId=' + e.userId + '&answerText=' + e.answerText;\n" +
"\n" +
"    webSocket = new WebSocket( url + '?' + params);\n" +
"    webSocket.binaryType = 'arraybuffer';\n" +
"\n" +
"    webSocket.onopen = function(message){\n" +
"        isConnected = true;\n" +
"        console.log( 'Server connected\\n' );\n" +
"    };\n" +
"\n" +
"    webSocket.onclose = function(message){\n" +
"        console.log( 'Server Disconnected\\n' );\n" +
"        closeWS();\n" +
"    };\n" +
"\n" +
"    webSocket.onerror = function(message){\n" +
"        console.log( 'error...\\n' );\n" +
"        isConnected = false;\n" +
"        closeWS();\n" +
"    };\n" +
"\n" +
"    webSocket.onmessage = function(message){\n" +
"        var jsonObj = JSON.parse(message.data);\n" +
"\n" +
"        /* STT 인식 알림 메세지 */\n" +
"        if(jsonObj.resCode == '183') {\n" +
"            console.log('Endpoint Detected. MSG=' + jsonObj);\n" +
"            sendNotiMsg(message);\n" +
"        }\n" +
"        /* 성공 혹은 실패 메세지 */\n" +
"        else {\n" +
"            sendMsg(message);\n" +
"        }\n" +
"        console.log( 'Receive From Server => '+message.data+'\\n' );\n" +
"    };\n" +
"    console.log('connect ws');\n" +
"}\n" +
"\n" +
"\n" +
"function closeWS() {\n" +
"    console.log('disconnect websocket');\n" +
"    webSocket.close();\n" +
"}\n" +
"\n" +
"\n" +
"\n" +
"/* ===================================================================================================================== */\n" +
"//\n" +
"/* ===================================================================================================================== */\n" +
"\n" +
"function sendNotiMsg(msg) {\n" +
"    try {\n" +
"        postMessage({\n" +
"            command: 'noti_stt',\n" +
"            message: msg.data\n" +
"        });\n" +
"    }\n" +
"    catch(e){    }\n" +
"    finally {    }\n" +
"}\n" +
"\n" +
"function sendMsg(msg) {\n" +
"\n" +
"    try {\n" +
"        postMessage({\n" +
"            command: 'response',\n" +
"            message: msg.data\n" +
"        });\n" +
"    }\n" +
"    catch(e){\n" +
"\n" +
"    }\n" +
"    finally {\n" +
"        isConnected = false;\n" +
"        closeWS();\n" +
"    }\n" +
"\n" +
"}\n" +
"\n" +
"\n" +
"/* ===================================================================================================================== */\n" +
"// 스트리밍 전송\n" +
"/* ===================================================================================================================== */\n" +
"\n" +
"function sendMessage(pcm){\n" +
"    if(isConnected == false) return;\n" +
"\n" +
"    var _buf = downsampleBuffer(pcm, 16000);\n" +
"    var view = encodeStreamWav(_buf, true);\n" +
"\n" +
"    webSocket.send(view);\n" +
"}\n" +
"\n" +
"/* ===================================================================================================================== */\n" +
"// 스트리밍을 위한 버퍼 데이타 관리\n" +
"/* ===================================================================================================================== */\n" +
"\n" +
"function clear(){\n" +
"    recLength = 0;\n" +
"    recBuffersL = [];\n" +
"    recBuffersR = [];\n" +
"}\n" +
"\n" +
"function getBuffers() {\n" +
"    console.log('Worker.getBuffers()');\n" +
"\n" +
"    var buffers = [];\n" +
"    buffers.push( mergeBuffers(recBuffersL, recLength) );\n" +
"    buffers.push( mergeBuffers(recBuffersR, recLength) );\n" +
"    this.postMessage(buffers);\n" +
"}\n" +
"\n" +
"function mergeBuffers(recBuffers, recLength){\n" +
"    var result = new Float32Array(recLength);\n" +
"    var offset = 0;\n" +
"    for (var i = 0; i < recBuffers.length; i++){\n" +
"        result.set(recBuffers[i], offset);\n" +
"        offset += recBuffers[i].length;\n" +
"    }\n" +
"    return result;\n" +
"}\n" +
"\n" +
"function downsampleBuffer(buffer, rate) {\n" +
"    if (rate == sampleRate) {\n" +
"        return buffer;\n" +
"    }\n" +
"    if (rate > sampleRate) {\n" +
"        throw 'downsampling rate show be smaller than original sample rate';\n" +
"    }\n" +
"    var sampleRateRatio = sampleRate / rate;\n" +
"    var newLength = Math.round(buffer.length / sampleRateRatio);\n" +
"    var result = new Float32Array(newLength);\n" +
"    var offsetResult = 0;\n" +
"    var offsetBuffer = 0;\n" +
"    while (offsetResult < result.length) {\n" +
"        var nextOffsetBuffer = Math.round((offsetResult + 1) * sampleRateRatio);\n" +
"        var accum = 0, count = 0;\n" +
"        for (var i = offsetBuffer; i < nextOffsetBuffer && i < buffer.length; i++) {\n" +
"            accum += buffer[i];\n" +
"            count++;\n" +
"        }\n" +
"        result[offsetResult] = accum / count;\n" +
"        offsetResult++;\n" +
"        offsetBuffer = nextOffsetBuffer;\n" +
"    }\n" +
"    return result;\n" +
"}\n" +
"\n" +
"function encodeStreamWav(samples, mono) {\n" +
"    var buffer = new ArrayBuffer(samples.length * 2);\n" +
"    var view = new DataView(buffer);\n" +
"    floatTo16BitPCM(view, 0, samples);\n" +
"    return view;\n" +
"}\n" +
"\n" +
"function floatTo16BitPCM(output, offset, input){\n" +
"    for (var i = 0; i < input.length; i++, offset+=2){\n" +
"        var s = Math.max(-1, Math.min(1, input[i]));\n" +
"        output.setInt16(offset, s < 0 ? s * 0x8000 : s * 0x7FFF, true);\n" +
"    }\n" +
"}\n" +
"\n" +
"\n" +
"function record(inputBuffer){\n" +
"    console.log('record', inputBuffer[0].length);\n" +
"\n" +
"    recBuffersL.push(inputBuffer[0]);\n" +
"    recBuffersR.push(inputBuffer[1]);\n" +
"    recLength += inputBuffer[0].length;\n" +
"    sendMessage(inputBuffer[0]);\n" +
"}"
/* --------------------------------------- */
        ], { type: 'text/javascript'});
        var worker = new Worker(window.URL.createObjectURL(blob));

        worker.addEventListener('message', handleMessageFromWorker);

        worker.postMessage({
            command: 'init',
            config: {
                sampleRate: this.context.sampleRate
            }
        });
        var recording = false;
        var currCallback;

        /* 오디오 캡쳐 */
        this.node.onaudioprocess = function(e){
            if (!recording) return;
            worker.postMessage({
                command: 'record',
                buffer: [
                    e.inputBuffer.getChannelData(0),
                    e.inputBuffer.getChannelData(1)
                ]
            });
        }

        this.configure = function(cfg){
            for (var prop in cfg){
                if (cfg.hasOwnProperty(prop)){
                    config[prop] = cfg[prop];
                }
            }
        }

        /*
        ** 서버 연동 시작
        */
        this.record = function(cmd, apiId, apiKey, userId, answerText){
            audioRecorder.clear();
            this.context.resume();
            worker.postMessage({
                command: 'openWS',
                config: {
                    cmd: cmd,
                    apiId: apiId,
                    apiKey: apiKey,
                    userId: userId,
                    answerText: answerText
                }
            });
            recording = true;
        }

        /*
        ** 서버 연동 중지
        */
        this.stop = function(){
            console.log("audioRecord.stop()");

            if(audioStream != null) audioStream.stop();
            audioStream = null;
            analyser = null;

            this.context.suspend();
            worker.postMessage({
                command: 'closeWS',
                config: {
                    sampleRate: this.context.sampleRate
                }
            });
            recording = false;
        }

        this.clear = function(){
            worker.postMessage({ command: 'clear' });
        }

        this.getBuffers = function(cb) {
            currCallback = cb || config.callback;
            worker.postMessage({ command: 'getBuffers' })
        }

        worker.onmessage = function(e){
            console.log("worker.onmessage()......");
        }

        source.connect(this.node);
        this.node.connect(this.context.destination);   // if the script node is not connected to an output the "onaudioprocess" event is not triggered in chrome.
    };


    window.Recorder = Recorder;

})(window);