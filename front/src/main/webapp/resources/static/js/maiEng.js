

/* =================================================================================================================== */
// 사용자 인터페이스
/* =================================================================================================================== */

var maiEng = {};

/*
** STT 수행
*/
maiEng.startStt = function(apiId, apiKey, userId, answerText, onResult_Stt, onFail) {
    if(audioStream != null) {
        console.log('error: 서비스가 이미 호출중입니다.');
        return;
    }

    maiEng.cmd = 'stt';

    maiEng.apiId = apiId;
    maiEng.apiKey = apiKey;
    maiEng.userId = userId;
    maiEng.answerText = answerText;

    maiEng.onResult_Stt = onResult_Stt;
    maiEng.onFail = onFail;

    openMic();
}

/*
** STT와 발음평가 동시 수행
*/
maiEng.startPron = function(apiId, apiKey, userId, answerText, onNoti_Stt, onResult_Pron, onFail) {
    if(audioStream != null) {
        console.log('error: 서비스가 이미 호출중입니다.');
        return;
    }

    maiEng.cmd = 'pron';

    maiEng.apiId = apiId;
    maiEng.apiKey = apiKey;
    maiEng.userId = userId;
    maiEng.answerText = answerText;

    maiEng.onNoti_Stt = onNoti_Stt;
    maiEng.onResult_Pron = onResult_Pron;
    maiEng.onFail = onFail;

    openMic();
}

/*
** STT와 파닉스 평가 동시 수행
*/
maiEng.startPhonics = function(apiId, apiKey, userId, answerText, onNoti_Stt, onResult_Phonics, onFail) {
    if(audioStream != null) {
        console.log('error: 서비스가 이미 호출중입니다.');
        return;
    }

    maiEng.cmd = 'phonics';

    maiEng.apiId = apiId;
    maiEng.apiKey = apiKey;
    maiEng.userId = userId;
    maiEng.answerText = answerText;

    maiEng.onNoti_Stt = onNoti_Stt;
    maiEng.onResult_Phonics = onResult_Phonics;
    maiEng.onFail = onFail;

    openMic();
}

/*
** 요청한 API 콜을 취소한다.
*/
maiEng.cancel = function() {
    audioRecorder.stop();
}

maiEng.isAvailableBrowser = function() {
    return hasGetUserMedia();
}
