package ai.mindslab.engedu_api.portal.common;

public class ResCode {
    public static final int SUCC = 200;
    public static final int STT_NOTI = 183;

    public static final int FAIL = -1;
    public static final int FAIL_TIMEOUT = -2;
}
