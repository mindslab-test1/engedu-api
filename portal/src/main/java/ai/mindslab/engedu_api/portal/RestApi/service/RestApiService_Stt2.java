package ai.mindslab.engedu_api.portal.RestApi.service;

import ai.mindslab.engedu_api.portal.api.ApiHandler_Stt;
import ai.mindslab.engedu_api.portal.common.Param;
import ai.mindslab.engedu_api.portal.common.Util_RecordVoice;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.Stt;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Slf4j
public class RestApiService_Stt2 {
    final int SAMPLE_RATE = 16000;
    final int TIMEOUT___DEFAULT = 12000;    // 이 값은 무시될 것임. 왜냐? 파라메타로 항상 넘어오니깐!
    final int TIMEOUT___NEXT = 100;         // Next() 호출 이후에 다음 Next() 대기 시간.

    /* 일반 설정 */
    int mTimeout = TIMEOUT___DEFAULT;

    /* RECORD URL 정보 */
    String mRecordUrl_Domain;
    int mRecordUrl_Port;

    /* GRPC */
    String mGrpcIp;
    int mGrpcPort;

    ManagedChannel mGrpc_Channel;
    SpeechToTextServiceGrpc.SpeechToTextServiceStub mGrpc_AsyncStub;
    StreamObserver<Stt.Speech> mGrpc_RequestObserver;

    boolean mFlag_GrpcTerminating = false;
    boolean mFlag_GrpcShutdown = false;
    boolean mFlag_Error = false;

    boolean mFlag_Next = false;         // 여러 문장을 인식할 경우, Next()가 여러번 호출됨.
                                        // 마지막 Next()를 알 수 없기 때문에 첫 Next() 호출 후 일정 시간만 대기함.
    long mNextTime = 0;

    /* STT 결과 데이터 */
    String mStt_ResultText = "";

    /* 음원 관련 변수 */
    Util_RecordVoice mRecordVoice = null;

    /* 결과 값 */
    HashMap<String, Object> mResult = new HashMap<String, Object>();

    /* REST API Parameter 정보 */
    String mApiId;
    String mApiKey;
    String mUserId;
    String mModel;
    String mAnswerText;
    MultipartFile mData;

    /* 백업그라운드 작업 대기 */
    CountDownLatch mFinishLatch;

    /* =========================================================================================================================== */
    // 생성 및 초기화
    /* =========================================================================================================================== */

    public RestApiService_Stt2(String apiId, String apiKey, String userId,
                               String model, String answerText,
                               String sttdIp, int sttdPort,
                               String recordUrlDomain, int recordUrlPort,
                               MultipartFile multipartFile,
                               int timeout) {
        mApiId = apiId;
        mApiKey = apiKey;
        mUserId = userId;
        mModel = model;
        mAnswerText = answerText;

        mGrpcIp = sttdIp;
        mGrpcPort = sttdPort;

        mRecordUrl_Domain = recordUrlDomain;
        mRecordUrl_Port = recordUrlPort;

        mData = multipartFile;

        mTimeout = timeout;
    }


    /* =========================================================================================================================== */
    // 서비스 실행
    /* =========================================================================================================================== */

    public HashMap<String, Object> execute() throws  Exception {
        mFlag_GrpcShutdown = false;
        mFlag_Error = false;

        mFlag_Next = false;
        mNextTime = 0;

        mRecordVoice = new Util_RecordVoice();
        mRecordVoice.createWavFile(mApiId, mUserId, false);
        long beginTime = System.currentTimeMillis();

        mFinishLatch = new CountDownLatch(1);

        initGrpc();
        sendGrpc(mData.getBytes());
        mGrpc_RequestObserver.onCompleted();

        mFinishLatch.await(mTimeout, TimeUnit.MILLISECONDS);
//        while (mFlag_Error == false && mFlag_GrpcShutdown == false && System.currentTimeMillis() - beginTime < mTimeout) {
//            Thread.sleep(10);
//
//            /* 더이상 Next가 호출 되지 않으면 연결을 종료한다. */
//            if(mFlag_GrpcTerminating == false && mFlag_Next && (System.currentTimeMillis() - mNextTime >= TIMEOUT___NEXT)) {
//                gracefulShutdown();
//                mFlag_GrpcTerminating = true;
//            }
//        }

        // TIMEOUT
        if(mFlag_GrpcShutdown == false && mFlag_Error == false) {
            log.error("@ STT >> TIMEOUT >> apiId={}, userId={}", mApiId, mUserId);

            mGrpc_Channel.shutdownNow();
            mRecordVoice.cancelWavFile();
            throw new Exception("grpc timeout");
        }
        // ERROR
        else if(mFlag_Error == true) {
            log.error("@ STT >> GRPC ERROR >> apiId={}, userId={}", mApiId, mUserId);

            mRecordVoice.cancelWavFile();
            throw new Exception("grpc server error");
        }

        /* 음원생성 종료 */
        mRecordVoice.finishWavFile();

        /* MP3 파일 생성 */
        String mp3FilePath = mRecordVoice.mFilePath;
        mp3FilePath = mp3FilePath.replace(".wav", ".mp3");
        mRecordVoice.writeWavToMp3(mRecordVoice.mFilePath, mp3FilePath);

        mRecordVoice = null;

        /* 결과 데이타 설정 */
        mResult.put(Param.ANSWER_TEXT, mAnswerText);
        mResult.put(Param.RESULT_TEXT, mStt_ResultText);
        mResult.put(Param.RECORD_URL, "https://" + mRecordUrl_Domain + ":" + mRecordUrl_Port + mp3FilePath);
        mResult.put(Param.SENTENCE_SCORE, ApiHandler_Stt.scoring(mAnswerText, mStt_ResultText));

        log.info("@ STT >> OK >> apiId={}, userId={} >> result={}", mApiId, mUserId, mResult.toString());
        return mResult;
    }


    /* =========================================================================================================================== */
    // GRPC 연동 (brain_stt)
    /* =========================================================================================================================== */

    public void initGrpc() throws IOException{

        log.info("=============================== sttService =================================");

        /* GRPC로 sttd 연결 및 설정 */
        mGrpc_Channel = ManagedChannelBuilder.forAddress(mGrpcIp, mGrpcPort).usePlaintext().build();
        mGrpc_AsyncStub = SpeechToTextServiceGrpc.newStub(mGrpc_Channel);

        Metadata meta = new Metadata();
        Metadata.Key<String> key1 = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> key2 = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> key3 = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);

        meta.put(key1, "eng");
        meta.put(key2, "" + SAMPLE_RATE);
        meta.put(key3, mModel);

        mGrpc_AsyncStub = MetadataUtils.attachHeaders(mGrpc_AsyncStub, meta);
        mGrpc_RequestObserver = mGrpc_AsyncStub.simpleRecognize(mGrpc_ResponseSimpleObserver);
    }


    StreamObserver<Stt.Text> mGrpc_ResponseSimpleObserver = new StreamObserver<Stt.Text>() {

        @Override
        public void onNext(Stt.Text value) {
            log.debug("@ mGrpc_ResponseObserver.onNext() >> apiId={}, userId={} >> {}", mApiId, mUserId, value.getTxt());

            /* 반환 메세지 */
            mStt_ResultText = value.getTxt().replace("\n", "");

            mFlag_Next = true;
            mNextTime = System.currentTimeMillis();

            mGrpc_Channel.shutdownNow();
            mFlag_GrpcShutdown = true;
            mFlag_Error = false;

            mFinishLatch.countDown();
        }

        @Override
        public void onError(Throwable t) {
            log.debug("@ mGrpc_ResponseObserver.onError() >> apiId=={}, userId={}", mApiId, mUserId);
            log.error("Error ================================== " + t.toString());

            mGrpc_Channel.shutdownNow();
            mFlag_GrpcShutdown = true;
            mFlag_Error = true;

            mFinishLatch.countDown();
        }

        @Override
        public void onCompleted() {
            log.debug("@ mGrpc_ResponseObserver.onCompleted() >> apiId={}, userId={}", mApiId, mUserId);
            mGrpc_Channel.shutdownNow();
            mFlag_GrpcShutdown = true;
            mFlag_Error = false;

            mFinishLatch.countDown();
        }
    };


    public void sendGrpc(byte[] data) {
        if(mFlag_GrpcShutdown) return;

        Stt.Speech speech = Stt.Speech.newBuilder().setBin(ByteString.copyFrom(data)).build();

        if (mGrpc_RequestObserver == null) {
            log.info("grpc is null@@@@@@@@@@@@@@@@@@@@@@@@@@");
            return;
        }

        if (speech == null) {
            log.info("speech is null@@@@@@@@@@@@@@@@@@@@@@");
            return;
        }

        mGrpc_RequestObserver.onNext(speech);

        /* 음원 기록 */
        mRecordVoice.writeWavFile(data);

        log.debug("@ STT : sendGrpc() >> send >> apiId={}, userId={}", mApiId, mUserId);
    }


    void gracefulShutdown() {
        if(mGrpc_Channel.isShutdown() || mGrpc_Channel.isTerminated()) return;
        mGrpc_RequestObserver.onCompleted();

        log.debug("@ ApiHandler_Stt.gracefulShutdown() >> apiId={}, userId={}", mApiId, mUserId);
    }
}
