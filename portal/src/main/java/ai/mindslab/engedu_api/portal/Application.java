package ai.mindslab.engedu_api.portal;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableAutoConfiguration
@ComponentScan
@Slf4j
public class Application extends SpringBootServletInitializer {
    @Value("${model.stt}")
    private String MODEL_DEFAULT;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        log.error("MODEL_DEFAULT ================================== >> {}", MODEL_DEFAULT);

        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}