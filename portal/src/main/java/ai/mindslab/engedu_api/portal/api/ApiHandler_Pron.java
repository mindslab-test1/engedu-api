package ai.mindslab.engedu_api.portal.api;

import ai.mindslab.eduai.pron_eval.EngeduPron;
import ai.mindslab.eduai.pron_eval.Service_EnglishPronEvalGrpc;
import ai.mindslab.engedu_api.portal.RestApi.service.RestApiService_Pron;
import ai.mindslab.engedu_api.portal.common.Param;
import com.sun.jndi.toolkit.url.Uri;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
public class ApiHandler_Pron extends ApiHandler {
    /* 발음평가 관련 정보 */
    String mRecPath;
    String mUtterance;

    float mSentenceScore;

    /* GRPC */
    String mRemoteIp;
    int mRemotePort;
    private ManagedChannel mGrpc_Channel;
    private Service_EnglishPronEvalGrpc.Service_EnglishPronEvalBlockingStub mGrpc_BlockingStub;

    Logger logger = LoggerFactory.getLogger(ApiHandler_Pron.class);

    public ApiHandler_Pron(WebSocketSession session, Callback cb, String ip, int port, String utterance, String recordUrl, int sentenceScore) {
        super(session, cb);

        mRemoteIp = ip;
        mRemotePort = port;

        mUtterance = utterance;

        mSentenceScore = (float)sentenceScore;

        try {
            Uri uri = new Uri(recordUrl);
            mRecPath = uri.getPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        log.debug("@ ApiHandler_Pron.onStart() >> ip={}, port={}", mRemoteIp, mRemotePort);

        mGrpc_Channel = ManagedChannelBuilder.forAddress(mRemoteIp, mRemotePort).usePlaintext().build();
        mGrpc_BlockingStub = Service_EnglishPronEvalGrpc.newBlockingStub(mGrpc_Channel);

        EngeduPron.Request_EnglishPronEval evaluationRequest = EngeduPron.Request_EnglishPronEval
                .newBuilder()
                .setUtterance(mUtterance)
                .setFilename(mRecPath)
                .setTransId("")
                .build();
        EngeduPron.Response_EnglishPronEval evaluationResponse = mGrpc_BlockingStub._evaluateEnglishPron(evaluationRequest);

        mGrpc_Channel.shutdown();

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put(Param.TOTAL_SCORE, calcScore ((float)convScore(evaluationResponse.getRegressionHolistic())) );
        result.put(Param.PRON_SCORE, calcScore ((float)evaluationResponse.getPronScore()) );
        result.put(Param.SPEED_SCORE, calcScore( (float)convScore(evaluationResponse.getRegressionSpeed())) );
        result.put(Param.RHYTHM_SCORE, calcScore ((float)convScore(evaluationResponse.getRegressionRhythm())) );
        result.put(Param.INTONATION_SCORE, calcScore ((float)convScore(evaluationResponse.getRegressionIntonation())) );
        result.put(Param.SEGMENTAL_SCORE, calcScore ((float)convScore(evaluationResponse.getRegressionSegmental())) );

        List<HashMap<String, Object>> words = new ArrayList<HashMap<String, Object>>();
        for(int xx = 0; xx < evaluationResponse.getListWordList().size(); xx++) {
            HashMap<String, Object> word = new HashMap<String, Object>();
            word.put(Param.WORD, evaluationResponse.getListWordList().get(xx).getName());
            word.put(Param.PRON_SCORE, evaluationResponse.getListWordList().get(xx).getPronScore());
            words.add(word);
        }
        result.put(Param.WORDS, words);

        log.debug("PRON: {}", result.toString());


        if(mCallback != null) mCallback.onPronResult(mSession, result);
    }

    /* 평가모듈에서 수신된 5점 만점에서 100 만점으로 점수 변환 */
    int convScore(float score) {
        return Math.round(score*10) * 2;
    }

    /* STT를 거친 문장 평가 점수를 반영하여 total_score 다시 계산 */
    float calcScore(float originScore) {
        float result;

        logger.info(" @ RestApiService_Pron ==> mSentenceScore = " + mSentenceScore + " / origin score = " + originScore );

        if(mSentenceScore <= 60)
            result = originScore - ( ((100 - mSentenceScore)/100) * (originScore/2) );
        else
            result = originScore;

        logger.info( " @ RestApiService_Pron ==> origin score = " + originScore + " / calcScore = " + Math.round(result) );

        return Math.round(result);
    }
}