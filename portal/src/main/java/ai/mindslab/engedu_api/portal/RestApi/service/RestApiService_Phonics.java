package ai.mindslab.engedu_api.portal.RestApi.service;

import ai.mindslab.engedu_api.portal.common.Param;
import com.sun.jndi.toolkit.url.Uri;
import engedu.eng_evaluation.EngeduPhonics;
import engedu.eng_evaluation.EvaluationServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;

import java.net.MalformedURLException;
import java.util.HashMap;

@Slf4j
public class RestApiService_Phonics {
    /* 파닉스 관련 정보 */
    String mRecPath;

    /* REST API Parameter 정보 */
    String mApiId;
    String mApiKey;
    String mUserId;
    String mModel;
    String mUtterance;
    String mAnswerWord;

    /* 결과값 */
    HashMap<String, Object> mResult = new HashMap<String, Object>();

    /* GRPC */
    String mGrpcIp;
    int mGrpcPort;

    private ManagedChannel mGrpc_Channel;
    private EvaluationServiceGrpc.EvaluationServiceBlockingStub mGrpc_BlockingStub;

    /* =========================================================================================================================== */
    // 생성 및 초기화
    /* =========================================================================================================================== */

    public RestApiService_Phonics(String apiId,
                                  String apiKey,
                                  String userId,
                                  String utterance,
                                  String answer_word,
                                  String recordUrl,
                                  String grpcIp,
                                  int grpcPort
    ) {
        mApiId = apiId;
        mApiKey = apiKey;
        mUserId = userId;
        mUtterance = utterance;
        mAnswerWord = answer_word;

        mGrpcIp = grpcIp;
        mGrpcPort = grpcPort;

        recordUrl = recordUrl.replace(".mp3", ".wav"); // 동일 경로에 존재하는 mp3를 wav로 변환한 것
        mRecPath = recordUrl;
    }

    public HashMap<String, Object> execute() throws Exception {
        Uri uri = new Uri(mRecPath);
        mRecPath = uri.getPath();

        System.out.println("mRecPath = " + mRecPath);
        System.out.println("Uri = " + uri);

        mGrpc_Channel = ManagedChannelBuilder.forAddress(mGrpcIp, mGrpcPort).usePlaintext().build();
        mGrpc_BlockingStub = EvaluationServiceGrpc.newBlockingStub(mGrpc_Channel);

        EngeduPhonics.PhonicsRequest evaluationRequest = EngeduPhonics.PhonicsRequest
                .newBuilder()
                .setUtterance(mUtterance)
                .setAnswerWord(mAnswerWord)
                .setFilename(mRecPath)
                .build();
        EngeduPhonics.PhonicsResponse phonicsResponse = mGrpc_BlockingStub.phonicsAnalyze(evaluationRequest);

        mGrpc_Channel.shutdown();

        HashMap<String, Object> result = new HashMap<String, Object>();
        phonicsResponse.getResultCode();
        result.put(Param.STATUS, phonicsResponse.getStatus());
        result.put(Param.USER_PRON, phonicsResponse.getUserPron());
        result.put(Param.CORRECT_PRON, phonicsResponse.getCorrectPron());

        System.out.println( phonicsResponse.getUserPron() );

        return result;
    }
}
