package ai.mindslab.engedu_api.portal.config;

import com.zaxxer.hikari.HikariDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.UUID;

@Configuration
public class DataSourceConfig {
    @ConfigurationProperties(prefix="spring.datasource")
    public DataSource dataSource() {
        DataSource datasource = (HikariDataSource) DataSourceBuilder.create().build();
        ((HikariDataSource) datasource).setPoolName("datasource_" + UUID.randomUUID().toString());
        return datasource;
    }
}
