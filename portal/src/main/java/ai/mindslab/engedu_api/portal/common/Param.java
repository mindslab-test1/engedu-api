package ai.mindslab.engedu_api.portal.common;

public class Param {

    public static final String ANSWER_TEXT = "answerText";
    public static final String ANSWER_WORD = "answerWord";
    public static final String API_ID = "apiId";

    public static final String CORRECT_PRON = "correctPron";

    public static final String INTONATION_LIST = "intonationList";
    public static final String INTONATION_SCORE = "intonationScore";

    public static final String MODEL = "model";

    public static final String PRON_SCORE = "pronScore";

    public static final String RECORD_URL = "recordUrl";
    public static final String RESULT_TEXT = "resultText";
    public static final String RES_CODE = "resCode";
    public static final String RESULT = "result";
    public static final String RHYTHM_SCORE = "rhythmScore";

    public static final String SEGMENTAL_SCORE = "segmentalScore";
    public static final String SENTENCE_SCORE = "sentenceScore";
    public static final String SPEED_SCORE = "speedScore";
    public static final String STATUS = "status";

    public static final String TOTAL_SCORE = "totalScore";

    public static final String USER_ID = "userId";
    public static final String USER_PRON = "userPron";

    public static final String WORD = "word";
    public static final String WORDS = "words";
}
