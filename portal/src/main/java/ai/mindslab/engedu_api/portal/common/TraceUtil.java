package ai.mindslab.engedu_api.portal.common;

import org.springframework.web.socket.WebSocketSession;

public class TraceUtil {
    public static String getTraceKey(WebSocketSession session) {
        return String.format("@apiId=%s / @userId=%s", session.getAttributes().get(Param.API_ID), session.getAttributes().get(Param.USER_ID));
    }
}
