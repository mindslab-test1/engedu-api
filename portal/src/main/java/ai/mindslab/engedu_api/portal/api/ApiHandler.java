package ai.mindslab.engedu_api.portal.api;

import org.springframework.web.socket.WebSocketSession;

import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.HashMap;

public class ApiHandler {

    WebSocketSession mSession;
    Callback mCallback = null;

    public ApiHandler(WebSocketSession session, Callback cb) {
        mSession = session;
        mCallback = cb;
    }

    /* ============================================================================================================ */
    // 기동 및 종료
    /* ============================================================================================================ */

    public void onStart() { }

    public void onStop() { }

    /* ============================================================================================================ */
    // 메세지 처리
    /* ============================================================================================================ */

    public void onMessage(ByteBuffer data) { }

    /* ============================================================================================================ */
    // Callback
    /* ============================================================================================================ */
    public interface Callback {
        public void onSttResult(WebSocketSession session, HashMap<String, Object> result);
        public void onPronResult(WebSocketSession session, HashMap<String, Object> result);
        public void onPhonicsResult(WebSocketSession session, HashMap<String, Object> result);
        public void onError(WebSocketSession session, int reason);
    }
}
