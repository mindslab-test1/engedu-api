package ai.mindslab.engedu_api.portal.api;

import ai.mindslab.engedu_api.portal.common.Param;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import maum.brain.stt.SpeechToTextServiceGrpc;
import maum.brain.stt.Stt;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.socket.WebSocketSession;

import javax.validation.constraints.Null;
import java.io.*;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

@Slf4j
public class ApiHandler_Stt extends ApiHandler {

    String mAnswerText;
    String mDomain;
    int    mPort_RecordUrl;

    /* STTD 연동 정보 */
    String mModel;
    int mSampleRate;

    /* GRPC */
    String mRemoteIp;
    int mRemotePort;
    ManagedChannel mGrpc_Channel;
    SpeechToTextServiceGrpc.SpeechToTextServiceStub mGrpc_AsyncStub;
    StreamObserver<Stt.Speech> mGrpc_RequestObserver;

    boolean mFlag_GrpcShutdown = false;

    /* STT 결과 데이타 */
    String mStt_ResultText = "";

    /* 음원 관련 변수 */
    String mBaseRecPath;
    String mWavFileName;
    String mMp3FileName;



    public ApiHandler_Stt(WebSocketSession session, Callback cb, String base_rec_path, String answer_text, String ip, int port, String model, int sampleRate, int totalTimeout, int epdTimeout, String domain,
                          int recordUrlPort) {
        super(session, cb);

        mAnswerText = answer_text;
        mDomain = domain;
        mPort_RecordUrl = recordUrlPort;

        mBaseRecPath = base_rec_path;

        mRemoteIp = ip;
        mRemotePort = port;
        mModel = model;
        mSampleRate = sampleRate;

        mTimeout_Connection = totalTimeout;
        mTimeout_EPD = epdTimeout;

        log.error("model = " + model);
        log.debug("Total time out = " + mTimer_Connection);
        log.debug("epd_time = " + mTimeout_EPD);
    }

    /* ================================================================================================================== */
    // 기능 시작
    /* ================================================================================================================== */

    @Override
    public void onStart() {
        log.debug("@ ApiHandler_Stt.onStart()");

        /* sttd와 GRPC로 연결 및 설정 */
        mGrpc_Channel = ManagedChannelBuilder.forAddress(mRemoteIp, mRemotePort).usePlaintext().build();
        mGrpc_AsyncStub = SpeechToTextServiceGrpc.newStub(mGrpc_Channel);

        Metadata meta = new Metadata();
        Metadata.Key<String> key1 = Metadata.Key.of("in.lang", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> key2 = Metadata.Key.of("in.samplerate", Metadata.ASCII_STRING_MARSHALLER);
        Metadata.Key<String> key3 = Metadata.Key.of("in.model", Metadata.ASCII_STRING_MARSHALLER);

        meta.put(key1, "eng");
        meta.put(key2, "" + mSampleRate);
        meta.put(key3, mModel);

        Metadata.Key<String> key4 = Metadata.Key.of("in.need.interim", Metadata.ASCII_STRING_MARSHALLER);
        meta.put(key4, "true");

        mGrpc_AsyncStub = MetadataUtils.attachHeaders(mGrpc_AsyncStub, meta);
        mGrpc_RequestObserver = mGrpc_AsyncStub.streamRecognize(mGrpc_ResponseObserver);

        startTimer_Connection();

        /* 음원 파일 생성 */
        createWavFile();
    }

    @Override
    public void onStop() {
        if(mGrpc_Channel.isShutdown()) return;

        log.debug("@ ApiHandler_Stt.onStop()");
        mGrpc_Channel.shutdownNow();
        mFlag_GrpcShutdown = true;

        // 에러 상황에서도 정상적인 파일 생성을 위해 호출함.
        finishWavFile();
    }


    StreamObserver<Stt.Segment> mGrpc_ResponseObserver = new StreamObserver<Stt.Segment>() {
        /* STTD로부터 결과 텍스트 수신 */
        @Override
        public void onNext(Stt.Segment value) {
            log.debug("@ mGrpc_ResponseObserver.onNext() >> {} >> {}", "ID", value.toString());

            startTimer_EPD();

            /* ##로 시작하면, 처리중인 텍스트 알림 메세지임 */
            if(value.getTxt().startsWith("##")) {

            }
            /* ##로 시작하지 않으면, 식별된 텍스트 반환 메세지임 */
            else {
                stopTimer_Connection();
                mStt_ResultText += value.getTxt();
            }
        }

        /* STTD와 연결 에러 발생 */
        @Override
        public void onError(Throwable t) {
            log.debug("@ mGrpc_ResponseObserver.onError() >> {}", "ID");
        }

        /* STTD와 연결 종료 */
        @Override
        public void onCompleted() {
            log.debug("@ mGrpc_ResponseObserver.onCompleted() >> {}", "ID");
            mGrpc_Channel.shutdownNow();
            mFlag_GrpcShutdown = true;

            /* 음원생성 종료 */
            finishWavFile();

            mMp3FileName = mWavFileName;
            mMp3FileName = mMp3FileName.replace(".wav", ".mp3");

            // MP3
            writeHandPcmMp3(mWavFileName, mMp3FileName);


            HashMap<String, Object> result = new HashMap<String, Object>();
            result.put(Param.ANSWER_TEXT, mAnswerText);
            result.put(Param.RESULT_TEXT, mStt_ResultText);
            result.put(Param.RECORD_URL, "https://" +mDomain + ":" + mPort_RecordUrl + mWavFileName);
            result.put(Param.SENTENCE_SCORE, scoring(mAnswerText, mStt_ResultText));
            if(mCallback != null) mCallback.onSttResult(mSession, result);

            log.info("@ STT RESULT >> {}", result.toString());

        }
    };

    /* ================================================================================================================== */
    // 클라이언트로 수신 메세지 처리
    /* ================================================================================================================== */

    @Override
    public void onMessage(ByteBuffer data) {
        if(mFlag_GrpcShutdown) return;

        byte[] byte_buff = data.array();
        log.debug("@ ApiHandler_Stt.onMessage() >> data = {}", byte_buff.length);
        if(byte_buff.length == 1) {
            gracefulShutdown();
        }
        else {
            Stt.Speech speech = Stt.Speech.newBuilder().setBin(ByteString.copyFrom(byte_buff)).build();
            mGrpc_RequestObserver.onNext(speech);

            /* 음원 기록 */
            writeWavFile(byte_buff);
        }
    }

    /* ================================================================================================================== */
    // 문장 평가 점수 계산
    /* ================================================================================================================== */
    static public int scoring(String answerText, String userText) {

        String[] answerSentence = answerText.split(" ");
        String[] userSentence = userText.split(" ");

        float eachScore;
        float resultScore = 0;
        int tmp = 0;

        for (int i=0 ; i<answerSentence.length ; i++) {
            answerSentence[i] = answerSentence[i].replace(".", "");
            answerSentence[i] = answerSentence[i].replace("?", "");
            answerSentence[i] = answerSentence[i].replace("!", "");
            answerSentence[i] = answerSentence[i].replace(",", "");
        }
        for (int i=0 ; i<userSentence.length ; i++) {
            userSentence[i] = userSentence[i].replace(".", "");
            userSentence[i] = userSentence[i].replace("?", "");
            userSentence[i] = userSentence[i].replace("!", "");
            userSentence[i] = userSentence[i].replace(",", "");
        }

        if(userSentence.length > answerSentence.length) {
            eachScore = 100 / (float) userSentence.length;
        } else {
            eachScore = 100 / (float) answerSentence.length;
        }

        for(int i=0 ; i<answerSentence.length ; i++) {
            for(int j=tmp ; j<userSentence.length ; j++) {
                if(answerSentence[i].equalsIgnoreCase(userSentence[j])) {
                    resultScore = resultScore + eachScore;
                    tmp = j+1;

                    break;
                }
            }
        }

        return (int) Math.round(resultScore);
    }


    /* ================================================================================================================== */
    // EPD 종료 처리
    /* ================================================================================================================== */

    void gracefulShutdown() {
        log.debug("@ ApiHandler_Stt.gracefulShutdown() >> {}", "ID");
        mFlag_GrpcShutdown = true;

        if(mGrpc_Channel.isShutdown()) return;
        mGrpc_RequestObserver.onCompleted();
    }

    /* ================================================================================================================== */
    // 전체 타이머
    /* ================================================================================================================== */

    int mTimeout_Connection = 0;
    Timer mTimer_Connection = null;
    TimerTask_Connection mTimerTask_Connection = null;

    class TimerTask_Connection extends TimerTask {
        public void run() {
            log.debug("@ ApiHandler_Stt.TimerTask_Connection >> timeout");
            stopTimer_Connection();

            //gracefulShutdown();
            try {
                throw new Exception("STT total Timeout");
            } catch (Exception e) {
                log.error("@ ApiHandler_Stt.TimerTask_Connection exception ! ==> " + e);
            }
        }
    }

    void startTimer_Connection() {
        log.debug("@ ApiHandler_Stt.startTimer_Connection()");
        if(mTimer_Connection != null) {
            try {
                mTimerTask_Connection.cancel();
                mTimer_Connection.purge();
            } catch (Exception e) {
                log.error("@ ApiHandler_Stt.startTimer_Connection exception ! ==> " + e);
            }
        }

        mTimer_Connection = new Timer(true);
        mTimerTask_Connection = new TimerTask_Connection();
        mTimer_Connection.schedule(mTimerTask_Connection, mTimeout_Connection);
    }

    void stopTimer_Connection() {
        log.debug("@ ApiHandler_Stt.stopTimer_Connection()");

        if(mTimer_Connection != null) {
            try {
                mTimerTask_Connection.cancel();
                mTimer_Connection.purge();
            } catch (Exception e) {
                log.error("@ ApiHandler_Stt.stopTimer_Connection exception ! ==> " + e);
            }
        }
        mTimerTask_Connection = null;
        mTimer_Connection = null;
    }

    /* ================================================================================================================== */
    // EPD 타이머
    /* ================================================================================================================== */

    int mTimeout_EPD = 0;
    Timer mTimer_EPD = null;
    TimerTask_EPD mTimerTask_EPD = null;

     class TimerTask_EPD extends TimerTask {
         public void run() {
             log.debug("@ ApiHandler_Stt.TimerTask_EPD >> timeout");
             stopTimer_EPD();

             gracefulShutdown();
         }
     }

     void startTimer_EPD() {
         log.debug("@ ApiHandler_Stt.startTimer_EPD()");
         if(mTimer_EPD != null) {
             try {
                 mTimerTask_EPD.cancel();
                 mTimer_EPD.purge();
             } catch (Exception e) {}
         }

         mTimer_EPD = new Timer(true);
         mTimerTask_EPD = new TimerTask_EPD();
         mTimer_EPD.schedule(mTimerTask_EPD, mTimeout_EPD);
     }

     void stopTimer_EPD() {
         log.debug("@ ApiHandler_Stt.stopTimer_EPD()");

         if(mTimer_EPD != null) {
             try {
                 mTimerTask_EPD.cancel();
                 mTimer_EPD.purge();
             } catch (Exception e) {}
         }
         mTimerTask_EPD = null;
         mTimer_EPD = null;
     }

    /* ================================================================================================================== */
    // 음성 Data 생성
    /* ================================================================================================================== */

    FileOutputStream mFileOutStream = null;
    DataOutputStream mOutStream = null;
    int mVoiceTotalSize = 0;

    /**
     * data를 받기 전, 비어 있는 파일(WAV format) 생성
     */
    public void createWavFile() {
        short  numChannels = 1; // mono
        int BITS_PER_SAMPLE = 16;

        /* 패스 */
        SimpleDateFormat fmtFolder = new SimpleDateFormat("yyyyMM");
        String path = mBaseRecPath + "/" + mSession.getAttributes().get(Param.API_ID) + "/" + mSession.getAttributes().get(Param.USER_ID) + "/" + fmtFolder.format(new Date());
        File file = new File(path);
        file.mkdirs();

        /* 파일명 */
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS");
        String today = fmt.format(new Date());
        mWavFileName = path + "/" +
                today + "-" +
                mSession.getAttributes().get(Param.API_ID) + "-" +
                mSession.getAttributes().get(Param.USER_ID) + ".wav";

        /* 파일 생성 */
        try {
            mFileOutStream = new FileOutputStream(mWavFileName);
            mOutStream = new DataOutputStream(mFileOutStream);

            // write the wav file per the wav file format
            mOutStream.writeBytes("RIFF"); // 00 - RIFF
            mOutStream.write(intToByteArray(32 + mVoiceTotalSize), 0, 4); // 04 - how big is the rest of this file? ==> Update 필요
            mOutStream.writeBytes("WAVE"); // 08 - WAVE
            mOutStream.writeBytes("fmt "); // 12 - fmt
            mOutStream.write(intToByteArray(16), 0, 4); // 16 - size of this chunk
            mOutStream.write(shortToByteArray((short) 1), 0, 2); // 20 - what is the audio format? 1 for PCM = Pulse Code M
            // dulation
            mOutStream.write(shortToByteArray(numChannels), 0, 2); // 22 - mono or stereo? 1 or 2? (or 5 or ???)
            mOutStream.write(intToByteArray(mSampleRate), 0, 4); // 24 - samples per second (numbers per second)
            mOutStream.write(intToByteArray((BITS_PER_SAMPLE / 8) * mSampleRate * numChannels), 0, 4); // 28 - bytes per second
            mOutStream.write(shortToByteArray((short) ((BITS_PER_SAMPLE / 8) * numChannels)), 0, 2); // 32 - # of bytes in one sample, for all channels
            mOutStream.write(shortToByteArray((short) BITS_PER_SAMPLE), 0, 2); // 34 - how many bits in a sample(number)? usually 16 or 24
            mOutStream.writeBytes("data"); // 36 - data
            mOutStream.write(intToByteArray(mVoiceTotalSize), 0, 4); // 40 - how big is this data chunk ==> Update 필요

        } catch (Exception e) {
            e.printStackTrace();

            try {
                if(mOutStream != null) mFileOutStream.close();
                mOutStream = null;

                if(mFileOutStream != null) mFileOutStream.close();
                mFileOutStream = null;

            } catch (Exception e2) {}
        }
    }


    /**
     *  Byte data를 받는대로 file에 append
     */

    public void writeWavFile(byte[] data) {
        if(mOutStream == null) return;

        try {

            // write the wav file per the wav file format
            mOutStream.write(data); // 44 - the actual data itself - just a long string of numbers
            mVoiceTotalSize += data.length;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * append할 data가 없으면 끝
     */

    public void finishWavFile() {
        try {
            if(mOutStream != null) mFileOutStream.close();
            mOutStream = null;

            if(mFileOutStream != null) mFileOutStream.close();
            mFileOutStream = null;


            // update가 필요한 header 수정
            RandomAccessFile raf = new RandomAccessFile(mWavFileName, "rw");

            raf.seek(4);
            raf.write(intToByteArray(32 + mVoiceTotalSize), 0, 4);

            raf.seek(40);
            raf.write(intToByteArray(mVoiceTotalSize), 0, 4);

        } catch (Exception e) {
          e.printStackTrace();
        }

    }


    /**
     * 형식 변환을 위해 필요한 utils
     */

    private byte[] intToByteArray(int i) {
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0x00FF);
        b[1] = (byte) ((i >> 8) & 0x000000FF);
        b[2] = (byte) ((i >> 16) & 0x000000FF);
        b[3] = (byte) ((i >> 24) & 0x000000FF);
        return b;
    }

    private byte[] shortToByteArray(short data) {
        return new byte[] { (byte) (data & 0xff), (byte) ((data >>> 8) & 0xff) };
    }



    public void writeHandPcmMp3(String wavName, String mp3Name) {

        Runtime rt = Runtime.getRuntime();
        Process pc = null;

        try {
            //외부 프로세스 실행
            // pc = rt.exec("lame -V2 --quiet "+wavName+" "+mp3Name);
            pc = rt.exec("lame -r -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name);
            log.debug("lame -r -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name + " start");
            pc.waitFor();
            pc.destroy();
            log.debug("lame -r -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name + " end");

        } catch (Exception e) {
            log.error("Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }

}
