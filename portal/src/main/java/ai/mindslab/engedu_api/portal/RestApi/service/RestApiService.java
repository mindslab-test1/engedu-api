package ai.mindslab.engedu_api.portal.RestApi.service;

import ai.mindslab.engedu_api.portal.common.Param;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@Slf4j
@Service
public class RestApiService {

    @Value("${timer.stt.timeout}")
    private int TIMEOUT___STT;

    @Value("${record_url.domain}")
    private String RECORD_URL___DOMAIN;

    @Value("${record_url.port}")
    private int RECORD_URL___PORT;

    @Value("${grpc.ip}")
    private String IP___GRPC;

    @Value("${stt.port}")
    private int PORT___STT;

    @Value("${pron.port}")
    private int PORT___PRON;

    @Value("${phonics.port}")
    private int PORT___PHONICS;

    @Value("${model.stt}")
    private String DEFAULT_MODEL___STT;

    @Value("${model.phonics}")
    private String DEFAULT_MODEL___PHONICS;

    /*
    ** STT 기능 수행
    */
    public HashMap<String, Object> execStt(String apiId, String apiKey, String userId, String model, String answerText, MultipartFile file) throws Exception {
        /* 모델 설정 */
        if(model == null || model.equals("null")) model = DEFAULT_MODEL___STT;
        else {
            // 허용된 모델인지 확인
        }

        RestApiService_Stt2 stt = new RestApiService_Stt2(apiId,
                                                        apiKey,
                                                        userId,
                                                        model,
                                                        answerText,
                                                        IP___GRPC,
                                                        PORT___STT,
                                                        RECORD_URL___DOMAIN,
                                                        RECORD_URL___PORT,
                                                        file,
                                                        TIMEOUT___STT
        );
        return stt.execute();
    }


    /*
    ** STT & 발음평가 기능 수행
    */
    public HashMap<String, Object> execPron(String apiId, String apiKey, String userId, String model, String answerText, MultipartFile file) throws Exception {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();

        /* 모델 설정 */
        if(model == null || model.equals("null") || model.equals("")) model = DEFAULT_MODEL___STT;
        else {
            // 허용된 모델인지 확인
        }

        /* STT 실행 */
        RestApiService_Stt2 stt = new RestApiService_Stt2(
                apiId,
                apiKey,
                userId,
                model,
                answerText,
                IP___GRPC,
                PORT___STT,
                RECORD_URL___DOMAIN,
                RECORD_URL___PORT,
                file,
                TIMEOUT___STT
        );
        resultMap = stt.execute();

        /* 발음평가 실행 */
        RestApiService_Pron pron = new RestApiService_Pron(
                apiId,
                apiKey,
                userId,
                (String) resultMap.get(Param.RESULT_TEXT),
                (String) resultMap.get(Param.RECORD_URL),
                IP___GRPC,
                PORT___PRON,
                (int) resultMap.get(Param.SENTENCE_SCORE)
        );
        resultMap.putAll(pron.execute());

        return resultMap;
    }

    /*
    ** STT & 파닉스평가 기능 수행
    */
    public HashMap<String, Object> execPhonics(String apiId, String apiKey, String userId, String model, String answer_word, MultipartFile file) throws Exception {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();

        /* 모델 설정 */
        if(model == null || model.equals("null") || model.equals("")) model = DEFAULT_MODEL___STT;
        else {
            // 허용된 모델인지 확인
        }

        /* STT 실행 */
        RestApiService_Stt2 stt = new RestApiService_Stt2(
                apiId,
                apiKey,
                userId,
                model,
                answer_word,
                IP___GRPC,
                PORT___STT,
                RECORD_URL___DOMAIN,
                RECORD_URL___PORT,
                file,
                TIMEOUT___STT
        );
        resultMap = stt.execute();

        RestApiService_Phonics phonics = new RestApiService_Phonics(
                apiId,
                apiKey,
                userId,
                (String) resultMap.get(Param.RESULT_TEXT),
                answer_word,
                (String) resultMap.get(Param.RECORD_URL),
                IP___GRPC,
                PORT___PHONICS
        );
        resultMap.putAll(phonics.execute());

        return resultMap;
    }
}
