package ai.mindslab.engedu_api.portal.api;


import ai.mindslab.engedu_api.portal.common.Param;
import ai.mindslab.engedu_api.portal.common.ResCode;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;


@Slf4j
@Component
public class ApiCenter {
    public static final String CMD = "cmd";

    public static final int CMD___STT = 0;
    public static final int CMD___STT_PRON = 1;
    public static final int CMD___STT_PHONICS = 2;

    public static final int CLOSE_STATUS___SEND_FAIL = -8001;

    @Value("${timer.stt.timeout}")
    private int TIMEOUT___TOTAL;

    @Value("${timer.stt.epd_detect.normal}")
    private int TIMEOUT___NORMAL_EPD;

    @Value("${timer.stt.epd_detect.phonics}")
    private int TIMEOUT___PHONICS_EPD;

    @Value("${record_url.domain}")
    private String RECORD_URL___DOMAIN;

    @Value("${stt.port}")
    private int STT___PORT;

    @Value("${stt.sample_rate}")
    private int STT___SAMPLE_RATE;

    @Value("${pron.port}")
    private int PRON___PORT;

    @Value("${phonics.port}")
    private int PHONICS___PORT;

    @Value("${model.stt}")
    private String DEFAULT_MODEL___STT;

    @Value("${model.phonics}")
    private String DEFAULT_MODEL___PHONICS;

    @Value("${stt.record_url.port}")
    private int RECORD_URL___PORT;

    HashMap<WebSocketSession, ApiHandler> mListAPI = new HashMap<WebSocketSession, ApiHandler>();

    Logger logger = LoggerFactory.getLogger(ApiCenter.class);

    /* ============================================================================================================ */
    // 세션 관리
    /* ============================================================================================================ */

    public void openSession(WebSocketSession session) {
        int cmd = (Integer) session.getAttributes().get(CMD);

        synchronized (mListAPI) {
            String model = (String) session.getAttributes().get(Param.MODEL);
            if(model == null || model.equals("null")) model = (cmd == CMD___STT_PHONICS ? DEFAULT_MODEL___PHONICS : DEFAULT_MODEL___STT);
            log.error("model =====================> {}", model);

            ApiHandler_Stt apiStt = new ApiHandler_Stt( session, mCallback,
                                                    "/record/engedu",
                                                    (String) session.getAttributes().get(Param.ANSWER_TEXT),
                                                    "127.0.0.1",
                                                    STT___PORT,
                                                    model,
                                                    STT___SAMPLE_RATE,
                                                    TIMEOUT___TOTAL,
                                                    (cmd == CMD___STT_PHONICS ? TIMEOUT___PHONICS_EPD : TIMEOUT___NORMAL_EPD),
                                                    RECORD_URL___DOMAIN,
                                                    RECORD_URL___PORT
            ) ;
            mListAPI.put(session, apiStt);

            log.debug("@ ApiCenter.openSession() >> session cnt = {}", mListAPI.size());
            apiStt.onStart();

        }

    }

    public void closeSession(WebSocketSession session) {
        ApiHandler api = removeApi(session);
        if(api != null) api.onStop();

        synchronized (mListAPI) {
            log.debug("@ ApiCenter.closeSession() >> session cnt = {}", mListAPI.size());
        }
    }

    ApiHandler removeApi(WebSocketSession session) {
        synchronized (mListAPI) {
            ApiHandler api = mListAPI.remove(session);
            return api;
        }
    }

    /* ============================================================================================================ */
    // 이벤트 콜백 처리
    /* ============================================================================================================ */

    ApiHandler.Callback mCallback = new ApiHandler.Callback() {
        @Override
        public void onSttResult(WebSocketSession session, HashMap<String, Object> result) {
            int cmd = (Integer) session.getAttributes().get(CMD);
            switch(cmd) {
                case CMD___STT:
                    try {
                        removeApi(session);
                        sendMessage(session, ResCode.SUCC, result);
                    } catch( Exception e) {
                        /* 메세지 전송 실패시 연결을 해지한다. 세션 삭제는 WsHandler에서 삭제 처리된다. */
                        try { session.close(CloseStatus.PROTOCOL_ERROR); } catch (Exception e2) {}
                    }

                    break;

                case CMD___STT_PRON:
                    try {
                        removeApi(session);
                        sendMessage(session, ResCode.STT_NOTI, result);
                    } catch( Exception e) {
                        /* 메세지 전송 실패시 연결을 해지한다. 세션 삭제는 WsHandler에서 삭제 처리된다. */
                        try { session.close(CloseStatus.PROTOCOL_ERROR); } catch (Exception e2) {}
                    }

                    ApiHandler_Pron apiPron = new ApiHandler_Pron(session, mCallback,
                            "127.0.0.1",
                            PRON___PORT,
                            (String) result.get(Param.RESULT_TEXT), (String) result.get(Param.RECORD_URL), (int) result.get(Param.SENTENCE_SCORE));
                    mListAPI.put(session, apiPron);
                    apiPron.onStart();
                    break;

                case CMD___STT_PHONICS:
                    try {
                        removeApi(session);
                        sendMessage(session, ResCode.STT_NOTI, result);
                    } catch( Exception e) {
                        /* 메세지 전송 실패시 연결을 해지한다. 세션 삭제는 WsHandler에서 삭제 처리된다. */
                        try { session.close(CloseStatus.PROTOCOL_ERROR); } catch (Exception e2) {}
                    }

                    ApiHandler_Phonics apiPhonics = new ApiHandler_Phonics(session, mCallback,
                            "127.0.0.1",
                            PHONICS___PORT,
                            (String) result.get(Param.RESULT_TEXT),
                            (String) result.get(Param.RECORD_URL),
                            (String) session.getAttributes().get(Param.ANSWER_TEXT));
                    mListAPI.put(session, apiPhonics);
                    apiPhonics.onStart();
                    break;
            }
        }

        @Override
        public void onPronResult(WebSocketSession session, HashMap<String, Object> result) {
            try {
                sendMessage(session, ResCode.SUCC, result);
            } catch (Exception e) {
                /* 메세지 전송 실패시 연결을 해지한다. 세션 삭제는 WsHandler에서 삭제 처리된다. */
                try { session.close(CloseStatus.PROTOCOL_ERROR); } catch (Exception e2) {}
            }
        }

        @Override
        public void onPhonicsResult(WebSocketSession session, HashMap<String, Object> result) {
            try {
                sendMessage(session, ResCode.SUCC, result);
            } catch (Exception e) {
                /* 메세지 전송 실패시 연결을 해지한다. 세션 삭제는 WsHandler에서 삭제 처리된다. */
                try { session.close(CloseStatus.PROTOCOL_ERROR); } catch (Exception e2) {}
            }
        }

        @Override
        public void onError(WebSocketSession session, int reason) {
            try {
                removeApi(session);
                sendMessage(session, reason, null);
            } catch( Exception e) {
                /* 메세지 전송 실패시 연결을 해지한다. 세션 삭제는 WsHandler에서 삭제 처리된다. */
                try { session.close(CloseStatus.PROTOCOL_ERROR); } catch (Exception e2) {}
            }
        }
    };


    /* ============================================================================================================ */
    // 메세지 처리
    /* ============================================================================================================ */

    /* 클라이언트로부터 수신된 메세지 처리 */
    public void onMessage(WebSocketSession session, ByteBuffer message) {
        synchronized (mListAPI) {
            ApiHandler api = mListAPI.get(session);
            if(api != null) api.onMessage(message);
        }
    }

    /* 클라이언트로 결과 메세지 전송 */
    void sendMessage(WebSocketSession session, int res_code, HashMap<String, Object> result) throws IOException {
        HashMap<String, Object> mapResponse = new HashMap<String, Object>();
        mapResponse.put(Param.RES_CODE, res_code);
        if(result != null) mapResponse.put(Param.RESULT, result);

        JSONObject json = new JSONObject();
        json.putAll( mapResponse );
        session.sendMessage(new TextMessage(json.toString()));
    }
}
