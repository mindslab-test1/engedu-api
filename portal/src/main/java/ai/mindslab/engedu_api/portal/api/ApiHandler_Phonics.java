package ai.mindslab.engedu_api.portal.api;

import ai.mindslab.engedu_api.portal.common.Param;
import com.sun.jndi.toolkit.url.Uri;
import engedu.eng_evaluation.EngeduPhonics;
import engedu.eng_evaluation.EvaluationServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.socket.WebSocketSession;

import java.net.MalformedURLException;
import java.util.HashMap;

@Slf4j
public class ApiHandler_Phonics extends ApiHandler {
    /* 발음평가 관련 정보 */
    String mRecPath;
    String mUtterance;
    String mAnswerWord;

    /* GRPC */
    String mRemoteIp;
    int mRemotePort;
    private ManagedChannel mGrpc_Channel;
    private EvaluationServiceGrpc.EvaluationServiceBlockingStub mGrpc_BlockingStub;


    public ApiHandler_Phonics(WebSocketSession session, Callback cb, String ip, int port, String utterance, String recordUrl, String answer_word) {
        super(session, cb);

        mRemoteIp = ip;
        mRemotePort = port;

        mUtterance = utterance;
        mAnswerWord = answer_word;

        try {
            Uri uri = new Uri(recordUrl);
            mRecPath = uri.getPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        log.debug("@ ApiHandler_Phonics.onStart() >> ip={}, port={}", mRemoteIp, mRemotePort);

        mGrpc_Channel = ManagedChannelBuilder.forAddress(mRemoteIp, mRemotePort).usePlaintext().build();
        mGrpc_BlockingStub = EvaluationServiceGrpc.newBlockingStub(mGrpc_Channel);

        EngeduPhonics.PhonicsRequest evaluationRequest = EngeduPhonics.PhonicsRequest
                .newBuilder()
                .setUtterance(mUtterance)
                .setFilename(mRecPath)
                .setAnswerWord(mAnswerWord)
                .build();
        EngeduPhonics.PhonicsResponse evaluationResponse = mGrpc_BlockingStub.phonicsAnalyze(evaluationRequest);

        mGrpc_Channel.shutdown();

        HashMap<String, Object> result = new HashMap<String, Object>();
        evaluationResponse.getResultCode();
        result.put(Param.STATUS, evaluationResponse.getStatus());
        result.put(Param.USER_PRON, evaluationResponse.getUserPron());
        result.put(Param.CORRECT_PRON, evaluationResponse.getCorrectPron());
        if(mCallback != null) mCallback.onPronResult(mSession, result);
    }
}
