package ai.mindslab.engedu_api.portal.websocket;


import ai.mindslab.engedu_api.portal.api.ApiCenter;
import ai.mindslab.engedu_api.portal.common.TraceUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.BinaryWebSocketHandler;

@Slf4j
@Component
public class WsHandler_Stt extends BinaryWebSocketHandler {

    @Autowired
    ApiCenter apiCenter;

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) throws Exception {
        apiCenter.onMessage(session, message.getPayload());
        super.handleBinaryMessage(session, message);
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        log.info("@ -----> WsHandler_Stt.afterConnectionEstablished() >> {} >> {}", TraceUtil.getTraceKey(session), session.getAttributes().toString());
        log.debug("#### {}", session.getHandshakeHeaders().toString());

        session.getAttributes().put(ApiCenter.CMD, ApiCenter.CMD___STT);
        apiCenter.openSession(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        log.info("@ <----- WsHandler_Stt.afterConnectionClosed() >> {} >> {}", TraceUtil.getTraceKey(session), session.getAttributes().toString());

        apiCenter.closeSession(session);
    }

}
