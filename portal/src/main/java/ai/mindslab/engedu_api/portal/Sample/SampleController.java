package ai.mindslab.engedu_api.portal.Sample;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
public class SampleController {

    @RequestMapping("/portal/sample")
    @ResponseBody
    public String sample() {

        log.error("portal hi...................... just error.");
        return "hello";
    }
}
