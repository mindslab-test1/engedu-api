package ai.mindslab.engedu_api.portal.RestApi.controller;

import ai.mindslab.engedu_api.portal.RestApi.service.RestApiService;
import ai.mindslab.engedu_api.portal.common.Param;
import ai.mindslab.engedu_api.portal.common.ResCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/engedu/v1")
@Component
public class RestApiController {

    @Autowired
    RestApiService restApiService;

    /*
    ** STT RestAPI
    */
    @RequestMapping(value="/stt", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String,Object>> restStt( @RequestParam("apiId") String apiId,
                                                           @RequestParam("apiKey") String apiKey,
                                                           @RequestParam("userId") String userId,
                                                           @RequestParam("model") String model,
                                                           @RequestParam("answerText") String answerText,
                                                           @RequestPart("file") MultipartFile file ) throws Exception {
        byte[] data = file.getBytes();
        log.info("@ Hello RestApiController.Stt >> apiId={}, apiKey={}, userId={}, model={}, answerText={}, file={} / {}{}{}{}",
                apiId, apiKey, userId, model, answerText, file.getSize(), data[0], data[1], data[2], data[3]);

        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        HashMap<String, Object> responseMap = new HashMap<String, Object>();

        try {
            dataMap.putAll(restApiService.execStt(apiId, apiKey, userId, model, answerText, file));
            dataMap.put(Param.RES_CODE, ResCode.SUCC);
        } catch(Exception e) {
            dataMap.put(Param.RES_CODE, ResCode.FAIL);
            log.error("@ STT : exception >> {} >> apiId={}, userId={}", e.getMessage(), apiId, userId);
        }
        responseMap.put(Param.RESULT, dataMap);

        return new ResponseEntity<HashMap<String, Object>>(responseMap, HttpStatus.OK);
    }

    /*
    ** PRON RestAPI
    */
    @RequestMapping(value="/pron", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> restPron( @RequestParam("apiId") String apiId,
                                                             @RequestParam("apiKey") String apiKey,
                                                             @RequestParam("userId") String userId,
                                                             @RequestParam("model") String model,
                                                             @RequestParam("answerText") String answerText,
                                                             @RequestPart("file") MultipartFile file ) throws Exception {
        byte[] data = file.getBytes();
        log.info("@ Hello RestApiController.Stt >> apiId={}, apiKey={}, userId={}, model={}, answerText={}, file={} / {}{}{}{}",
                apiId, apiKey, userId, model, answerText, file.getSize(), data[0], data[1], data[2], data[3]);

        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        HashMap<String, Object> responseMap = new HashMap<String, Object>();

        try {
            dataMap.putAll(restApiService.execPron(apiId, apiKey, userId, model, answerText, file));
            dataMap.put(Param.RES_CODE, ResCode.SUCC);
        } catch(Exception e) {
            dataMap.put(Param.RES_CODE, ResCode.FAIL);
            log.error("@ STT : exception >> {} >> apiId={}, userId={}", e.getMessage(), apiId, userId);
        }
        responseMap.put(Param.RESULT, dataMap);

        return new ResponseEntity<HashMap<String, Object>>(responseMap, HttpStatus.OK);
    }

    /*
    ** PHONICS RestAPI
    */
    @RequestMapping(value = "/phonics", method = RequestMethod.POST)
    public ResponseEntity<HashMap<String, Object>> restPhonics( @RequestParam("apiId") String apiId,
                                                                @RequestParam("apiKey") String apiKey,
                                                                @RequestParam("userId") String userId,
                                                                @RequestParam("model") String model,
                                                                @RequestParam("answerText") String answerText,              // 차후엔 삭제 예정
                                                                @RequestPart("file") MultipartFile file) throws Exception {
        byte[] data = file.getBytes();
        log.info("@ Hello RestApiController.Phonics >> apiId={}, apiKey={}, userId={}, model={}, answerText={}, file={} / {}{}{}{}",
                apiId, apiKey, userId, model, answerText, file.getSize(), data[0], data[1], data[2], data[3]);

        HashMap<String, Object> dataMap = new HashMap<String, Object>();
        HashMap<String, Object> responseMap = new HashMap<String, Object>();

        try {
            dataMap.putAll(restApiService.execPhonics(apiId, apiKey, userId, model, answerText, file));
            dataMap.put(Param.RES_CODE, ResCode.SUCC);
        } catch(Exception e) {
            dataMap.put(Param.RES_CODE, ResCode.SUCC);
            log.error("@ STT : exception >> {} >> apiId={}, userId={}", e.getMessage(), apiId, userId);
        }
        responseMap.put(Param.RESULT, dataMap);

        return new ResponseEntity<HashMap<String, Object>>(responseMap, HttpStatus.OK);
    }
}
