package ai.mindslab.engedu_api.portal.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class Util_RecordVoice {

    public FileOutputStream mFileOutStream = null;
    public DataOutputStream mOutStream = null;
    public String           mFilePath = null;
    int                     mTotalSize = 0;

    /*
    ** WAV 파일 생성
    */
    public void createWavFile(
                                String apiId,
                                String userId,
                                boolean createHeaderFlag
    ) {
        final short NUM_CHANNEL = 1;
        final int BITS_PER_SAMPLE = 16;
        final int SAMPLE_RATE = 16000;
//        final int SAMPLE_RATE = 48000;

        /* 초기화 */
        try {
            if (mFileOutStream != null) mFileOutStream.close();
            if (mOutStream != null) mOutStream.close();
        } catch(Exception e) {}
        mFileOutStream = null;
        mOutStream = null;
        mTotalSize = 0;

        /* 경로 */
        SimpleDateFormat fmtFolder = new SimpleDateFormat("yyyyMM");
        String path = ConstDef.REC_VOICE___BASE_PATH + "/" + apiId + "/" + userId + "/" + fmtFolder.format(new Date());
        File file = new File(path);
        file.mkdirs();

        /* 파일명 */
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS");
        String today = fmt.format(new Date());
        mFilePath = path + "/" +
                today + "-" +
                apiId + "-" +
                userId + ".wav";

        /* 파일 생성 */
        try {
            mFileOutStream = new FileOutputStream(mFilePath);
            mOutStream = new DataOutputStream(mFileOutStream);

            // wav 포맷 header 생성 (파일을 직접 받을 시, wav header 필요)
            if(createHeaderFlag) {
                mOutStream.writeBytes("RIFF"); // 00 - RIFF
                mOutStream.write(intToByteArray(0 /* temp */), 0, 4); // 04 - how big is the rest of this file? ==> Update 필요
                mOutStream.writeBytes("WAVE"); // 08 - WAVE
                mOutStream.writeBytes("fmt "); // 12 - fmt
                mOutStream.write(intToByteArray(16), 0, 4); // 16 - size of this chunk
                mOutStream.write(shortToByteArray((short) 1), 0, 2); // 20 - what is the audio format? 1 for PCM = Pulse Code M
                mOutStream.write(shortToByteArray(NUM_CHANNEL), 0, 2); // 22 - mono or stereo? 1 or 2? (or 5 or ???)
                mOutStream.write(intToByteArray(SAMPLE_RATE), 0, 4); // 24 - samples per second (numbers per second)
                mOutStream.write(intToByteArray((BITS_PER_SAMPLE / 8) * SAMPLE_RATE * NUM_CHANNEL), 0, 4); // 28 - bytes per second
                mOutStream.write(shortToByteArray((short) ((BITS_PER_SAMPLE / 8) * NUM_CHANNEL)), 0, 2); // 32 - # of bytes in one sample, for all channels
                mOutStream.write(shortToByteArray((short) BITS_PER_SAMPLE), 0, 2); // 34 - how many bits in a sample(number)? usually 16 or 24
                mOutStream.writeBytes("data"); // 36 - data
                mOutStream.write(intToByteArray(0 /* temp */), 0, 4); // 40 - how big is this data chunk ==> Update 필요
            }
        } catch (Exception e) {
            e.printStackTrace();

            try {
                if(mOutStream != null) mOutStream.close();
                mOutStream = null;

                if(mFileOutStream != null) mFileOutStream.close();
                mFileOutStream = null;

            } catch (Exception e2) {}
        }
    }


    /*
    ** WAV 파일에 데이타 기록\
    */
   public void writeWavFile( byte[] data ) {
        if(mOutStream == null) return;

        try {
            // write the wav file per the wav file format
            mOutStream.write(data); // 44 - the actual data itself - just a long string of numbers
            mTotalSize += data.length;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
    ** WAV 파일 기록 종료 처리
    */

    public void finishWavFile( ) {
        finishWavFile(false, null);
    }

    public void finishWavFile(  boolean updateFlag,
                                String fileName
    ) {
        try {
            if(mOutStream != null) mOutStream.close();
            mOutStream = null;

            if(mFileOutStream != null) mFileOutStream.close();
            mFileOutStream = null;

            if(updateFlag) {
                // update가 필요한 header 수정
                RandomAccessFile raf = new RandomAccessFile(fileName, "rw");

                raf.seek(4);
                raf.write(intToByteArray(32 + mTotalSize), 0, 4);

                raf.seek(40);
                raf.write(intToByteArray(mTotalSize), 0, 4);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
    ** 기록중인 WAV 파일을 삭제한다.
    */
    public void cancelWavFile() {
        try {
            if(mOutStream != null) mOutStream.close();
            mOutStream = null;

            if(mFileOutStream != null) mFileOutStream.close();
            mFileOutStream = null;

            File file = new File(mFilePath);
            file.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* ======================================================================================================================= */
    // 정수형 -> 바이트 배열
    /* ======================================================================================================================= */

    /*
    ** INT -> BYTE []
    */
    private byte[] intToByteArray(int i) {
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0x00FF);
        b[1] = (byte) ((i >> 8) & 0x000000FF);
        b[2] = (byte) ((i >> 16) & 0x000000FF);
        b[3] = (byte) ((i >> 24) & 0x000000FF);
        return b;
    }

    /*
     ** SHORT -> BYTE []
     */
    private byte[] shortToByteArray(short data) {
        return new byte[] { (byte) (data & 0xff), (byte) ((data >>> 8) & 0xff) };
    }


    /* ======================================================================================================================= */
    //
    /* ======================================================================================================================= */

    public void writeWavToMp3(String wavName, String mp3Name) {

        Runtime rt = Runtime.getRuntime();
        Process pc = null;

        try {
            //외부 프로세스 실행
            pc = rt.exec("lame -r -s16000 -b128 -m m -V2 --quiet "+wavName+" "+mp3Name);
            pc.waitFor();
            pc.destroy();

        } catch (Exception e) {
            log.error("Exception : " + e.getMessage());
            e.printStackTrace();
        }
    }
}
