package ai.mindslab.engedu_api.portal.config;


import ai.mindslab.engedu_api.portal.websocket.WsHandler_Phonics;
import ai.mindslab.engedu_api.portal.websocket.WsHandler_Pron;
import ai.mindslab.engedu_api.portal.websocket.WsHandler_Stt;
import ai.mindslab.engedu_api.portal.websocket.WsHandshakeInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Slf4j
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {



	@Autowired
	private WsHandler_Stt stt;

	@Autowired
	private WsHandler_Pron pron;

	@Autowired
	private WsHandler_Phonics phonics;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		log.debug("@ WebSocketConfig.registerWebSocketHandlers()");
		log.info("@ WebSocketConfig.registerWebSocketHandlers()");
		log.error("@ WebSocketConfig.registerWebSocketHandlers()");

		registry.addHandler(stt, "/engedu/websocket/stt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(pron, "/engedu/websocket/pron").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(phonics, "/engedu/websocket/phonics").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");

		// register WebSocket Handler
		registry.addHandler(stt, "/engedu/v1/websocket/stt").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(pron, "/engedu/v1/websocket/pron").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
		registry.addHandler(phonics, "/engedu/v1/websocket/phonics").addInterceptors(new WsHandshakeInterceptor()).setAllowedOrigins("*");
	}
}
